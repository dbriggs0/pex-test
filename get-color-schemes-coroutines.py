#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      doncb
#
# Created:     03/12/2017
# Copyright:   (c) doncb 2017
# Licence:     <your licence>
#-------------------------------------------------------------------------------
from io import BytesIO
import itertools
from PIL import Image
import requests


def get_max(depth=1):
    pixel_counts = {}
    next_pixel = yield
    while True:
        if next_pixel == -1:
            maxes = sorted(pixel_counts, key=pixel_counts.get, reverse=True)[:depth]
            for i in range(len(maxes)):
                maxes[i] = str(maxes[i]).replace(',','_')
            next_pixel = yield maxes
            pixel_counts = {}
        else:
            try:
                pixel_counts[next_pixel] += 1
            except KeyError:
                pixel_counts[next_pixel] = 1
            next_pixel = yield



def get_pixels():
    i = 0
    sent_image = yield
    while True:
        if sent_image:
            width, height = sent_image.size
            x, y = 0,0
            image = sent_image.convert('RGB')
            sent_image = None
        else:
            if y == height:
                sent_image = yield 'Got all pixels'
            else:
                #print('x, y, pixel', x, y, image.getpixel((x,y)))
                yield (image.getpixel((x,y)))
                x += 1
                if x == width:
                    x = 0
                    y += 1

# Read in file
# Get pixel
# add pixel to tally
# repeat until through with all pixels
# return top 3

def main():
    with open('color-schemes-coroutines.csv', 'a+') as schemes_file:
        with open('urls.txt', 'r') as urls_file:
            get_pixs = get_pixels()
            next(get_pixs)
            get_maxes = get_max(3)
            next(get_maxes)
            i = 0
            for image_url in urls_file:
                image_data = requests.get(image_url)
                with Image.open(BytesIO(image_data.content)) as image:
                    get_pixs.send(image)
                    next_pix = next(get_pixs)
                    while next_pix != 'Got all pixels':
                        get_maxes.send(next_pix)
                        next_pix = next(get_pixs)
                    top_three = get_maxes.send(-1)
                    schemes_file.write('%s; %s; %s; %s\n' % (str(image_url), top_three[0], top_three[1], top_three[2]))
            get_pixs.close()
            get_maxes.close()


if __name__ == '__main__':
    main()