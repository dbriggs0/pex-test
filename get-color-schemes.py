#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      doncb
#
# Created:     03/12/2017
# Copyright:   (c) doncb 2017
# Licence:     <your licence>
#-------------------------------------------------------------------------------
from io import BytesIO
import itertools
from memory_profiler import profile
from PIL import Image
import requests

class PixelCounter():
    def __init__(self):
        self.pixel_counts = {}

    def add_pixel(self, rgb_value):
        try:
            self.pixel_counts[rgb_value] += 1
        except KeyError:
            self.pixel_counts[rgb_value] = 1

    def get_max(self, depth=1):
        maxes = sorted(self.pixel_counts, key=self.pixel_counts.get, reverse=True)[:depth]
        for i in range(len(maxes)):
            maxes[i] = str(maxes[i]).replace(',','-')
        return maxes
@profile
def main():
    with open('color-schemes.csv', 'a+') as schemes_file:
        with open('urls.txt', 'r') as urls_file:
            for image_url in urls_file:
                image_data = requests.get('https://i.redd.it/lsuw4p2ncyny.jpg')
                print('Got the image data')
                with Image.open(BytesIO(image_data.content)) as image:
                    print('opened the image')
                    pix_counter = PixelCounter()
                    width, height = image.size
                    pixels = ((image.getpixel((x,y))) for x,y in itertools.product(range(width), range(height)))
                    for pixel in pixels:
                        pix_counter.add_pixel(pixel)
                    top_three = pix_counter.get_max(3)
                    print('Writing to file')
                    schemes_file.write('%s; %s; %s; %s\n' % (str(image_url), top_three[0], top_three[1], top_three[2]))
                    break
        print('Everything Finished!')
if __name__ == '__main__':
    main()